//
//  ViewController.swift
//  Swift Sample - v2
//
//  Created by Zain Sajjad on 19/03/2019.
//  Copyright © 2019 Zain Sajjad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var loadingView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func initializePeekaboo(params: NSDictionary) {
        if (loadingView == nil) {
            let sb:UIStoryboard = UIStoryboard.init(name: "LaunchScreen", bundle: nil);
            let uvc:UIViewController = sb.instantiateInitialViewController()!;
            loadingView = uvc.view;
            var frame:CGRect = self.view.frame;
            frame.origin = CGPoint(x:0, y:0);
            loadingView.frame = frame;
            loadingView.layer.zPosition = 1;
        }
        
        
        let currentWindow:UIWindow = UIApplication.shared.keyWindow!;
        currentWindow.addSubview(loadingView);

        let uivc:UIViewController = Peekaboo.getPeekabooViewController(params: params);
        uivc.modalPresentationStyle = UIModalPresentationStyle.fullScreen;
        self.present(uivc, animated: true, completion: {() -> Void in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(
                    withDuration: 0.2,
                    animations: {self.loadingView.alpha = 0.0},
                    completion: {(value: Bool) in
                                    self.loadingView.removeFromSuperview();
                                }
                )
            }
        });
    }
    
    @IBAction func startDeals() {
        let params:NSDictionary = [
            "type": "deals",
        ];
        initializePeekaboo(params: params);
    }
    
    
    @IBAction func startLocator() {
        let params:NSDictionary = [
            "type": "locator",
        ];
        initializePeekaboo(params: params);
    }
    
    @IBAction func startCarFinance() {
        let params:NSDictionary = [
            "type": "carFinance",
        ];
        initializePeekaboo(params: params);
    }
    
    @IBAction func startInstalment() {
        let params:NSDictionary = [
            "type": "instalment",
        ];
        initializePeekaboo(params: params);
    }

    
    @IBAction func startTransaction() {
        let params:NSDictionary = [
            "type": "transaction",
            "userId": "73",
            "enableTransaction": true
        ];
        initializePeekaboo(params: params);
    }

}

