//
//  Peekaboo.swift
//  Swift Sample - v2
//
//  Created by Zain Sajjad on 19/03/2019.
//  Copyright © 2019 Zain Sajjad. All rights reserved.
//

import Foundation

let peekaboo:PeekabooConnect = PeekabooConnect.init();
var isInitialized : Bool = false;
class Peekaboo {
    class func initialize() {
        peekaboo.initializePeekaboo();
        isInitialized = true;
    }
    
    class func getPeekabooViewController(params: NSDictionary) -> UIViewController {
        if (!isInitialized) {
            initialize();
        }
        let fullParams:NSMutableDictionary = [
            "environment": "production",
            "pkbc": "app.com.brd",
//            "pkbc": "com.efulife.customer",
            "userId": "72",
            "association": "1",
            "enableRedemption": true,
            "country": "Pakistan",
            "initialRoute": "",
        ];
        fullParams.addEntries(from: params as! [AnyHashable : Any]);
        return peekaboo.getPeekabooUIViewController(fullParams as? [AnyHashable : Any]);
    }
}
